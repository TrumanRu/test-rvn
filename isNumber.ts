export function isNumber1(value: unknown | null): boolean {
  return (value != null) && !Array.isArray(value) && !Number.isNaN(Number(value)); // Number(null) = 0
}

export function isNumber2(value: unknown | null): boolean {
  return !Array.isArray(value) && /^-?\d+$/.test(String(value));
}

function cloneArray(inputArray: any[]) {
  return ([] as any[]).concat(inputArray);
}

function checkTime(label: string, repeat: number, fn: Function, payload: any[], showValues = false) {
  console.time(label);
  for (let i = 1; i <= repeat; i++) {
    payload.forEach((data) => {
      if (showValues) {
        console.log(data.input, '=', Number(data.input), '=>', fn(data.input));
      }
    });
  }
  console.timeEnd(label);
}

type DataItem = {
  input: any;
  result: boolean;
};

const DATA_LIST = [
  { input: 1, result: true },
  { input: '2', result: true },
  { input: -3, result: true },
  { input: '-4', result: true },
  { input: 0, result: true },
  { input: '0', result: true },
  { input: 'str1', result: false },
  { input: '2str', result: false },
  { input: /2/g, result: false },
  // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  { input: [5], result: false },
  { input: ['6'], result: false },
  { input: { 7: 77 }, result: false },
  { input: undefined, result: false },
  { input: null, result: false },
];

function generateData(size: number): DataItem[] {
  const values: DataItem[] = [];
  for (let i = 1; i <= size; i++) {
    let value: string | undefined;
    switch (parseInt((Math.random() * 10).toString())) {
      case 0:
        value = '';
        break;
      case 1:
        value = undefined;
        break;
      default:
        value = parseInt((Math.random() * 100).toString()).toString();
    }
    values.push({
      input: value,
      result: true,
    });
  }
  return values;
}

checkTime('isNumber1_values', 1, isNumber1, DATA_LIST, true);
checkTime('isNumber2_values', 1, isNumber2, DATA_LIST, true);

const data1 = generateData(10000);
const data2 = generateData(10000);

checkTime('isNumber1', 100000, isNumber1, data1);
checkTime('isNumber2', 100000, isNumber2, data2);









