const http = require('http');

/**
 *
 * @param {array[string]} synonims
 * @returns {string}
 */
function getParameterValue(synonims) {
  let position;
  synonims.findIndex((option) => {
    const pos = process.argv.indexOf(option);
    if (pos !== -1) {
      position = pos;
      return pos;
    }
  });
  return position
    ? process.argv[position + 1]
    : undefined;
}

function processor(req, res) {
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify({
    success: true,
  }));
  console.log('Request responded.')
}

const port = getParameterValue(['--port', '-p']) ?? 3031;
const delay = - -(getParameterValue(['--delay', '-d']) ?? 0);

console.log(`Port: ${port}`);
console.log(`Delay: ${delay}s`);

const server = http.createServer((req, res) => {
  console.log('\nRequest received...')
  setTimeout(processor, delay * 1000, req, res);
});

server.listen(port);
