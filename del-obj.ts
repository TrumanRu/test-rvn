const obj1: any = {
  prop1: '11111',
}

if (new Date() > new Date('2022-11-11')) {
  obj1.prop2 = '22222';
}

delete obj1.prop2;
delete obj1.prop3;
