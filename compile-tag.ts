const REPEAT = 10000;

const regExp1 = RegExp('^var\.global\.', 'i');

const data1 = [
  'var.global.var1',
  'var.global.var2',
  'var.any'
];

function search(data: string[], regExp: RegExp) {
  for (let i = 1; i <= REPEAT; i++) {
    const result = data.filter((s) => s.search(regExp) === 0);
  }
}

function test(data: string[], regExp: RegExp) {
  for (let i = 1; i <= REPEAT; i++) {
    const result = data.filter((s) => regExp.test(s));
  }
}

search(data1, regExp1);
test(data1, regExp1);

console.time('test');
test(data1, regExp1);
console.timeEnd('test');


console.time('search');
search(data1, regExp1);
console.timeEnd('search');
