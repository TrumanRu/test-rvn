const TAG_PREFIX = 'var.global';

const inputTags = [
  'var.global.var1.subvar1',
  'var.global.VAR2',
  'VaR.gLoBaL.Var3',
  'samovar.global.var3',
  'tupper.var.global.var4',
];

export function extractTag(rawTag: string): string | undefined {
  const result = rawTag.match(new RegExp(`^${TAG_PREFIX}.(.+)`, 'i'));

  return result?.[1];
}

const result = inputTags.map((tag) => extractTag(tag));

console.log(result);
