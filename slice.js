const array1 = [
  'first',
  'second',
  'third',
  'fourth',
  'firth',
  'sixth',
  'seventh',
  'eighth',
  'ninth',
  'tenth',
  'eleventh',
  'twelfth',
  'thirtieth',
  'fortieth',
  'fiftieth',
  'sixtieth',
  'seventieth',
  'eightieth',
  'ninetieth',
  'twentieth',
  'twenty first',
];

const array2 = [];

const packetSize = 6;
let currentPos = 0;

console.log('PACKET SIZE:', packetSize, '> 0', packetSize > 0);

while (array1.length > currentPos) {
  const subArray = array1.slice(currentPos, (currentPos += packetSize));
  console.log(subArray);
}

console.log('DONE');
