const data = [
  new Date(),
  [1, 2, 4],
  'STRING',
  455,
  undefined,
  null,
  new Map([['a', 1]]),
];

export function getType(value: unknown): string {
  const type = Object.prototype.toString.call(value);
  return type.match(/^\[object (.+)\]$/)?.[1].toLowerCase() || type;
}

data.forEach((v) => {
  console.log(getType(v));
});
