interface TagDescription {
  prefix: string;
  name: string;
}

const prefixes = [
    'var.global',
    'var.local',
];

const inputTags = [
  'var.global.var1.subvar1',
  'var.global.var2',
  'samovar.global.var3',
  'tupper.var.global.var4',
];

export function extractTag(
  acceptPrefixes: string[],
  fullTag: string,
): TagDescription | undefined {
  let tagName: string | undefined;
  const resultPrefix = acceptPrefixes.find((prefix: string) => {
    const regexp = new RegExp(`(^${prefix.replace('.', '\\.')})\\.(.+)`);
    const matches = fullTag.match(regexp);

    if (matches) {
      tagName = matches[2];
      return true;
    }
  });
  return resultPrefix
    ? ({ name: tagName, prefix: resultPrefix } as TagDescription)
    : undefined;
}

const result = inputTags.map((tag) => extractTag(prefixes, tag));

console.log(result);
