const express = require('express');

const app = express();

app.post('/', (req, res) => {
  console.log(req);
  res.statusCode = 200;
  res.statusMessage = 'OK';
  res.send('Hello!');
});

app.listen(3032);
