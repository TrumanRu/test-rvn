
const decrypt = (value: string) => {
  return value.substring(0, value.length - '_encrypted'.length);
};

console.log(decrypt('value_encrypted'));
