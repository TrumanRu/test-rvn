const serviceTags = ['var.global'];
const tags = ['var.global.var1', 'var.other.var2', 'samovar.global.var3', 'VAR.global.var4'];


const result = tags.filter((tag) => serviceTags.some((sTag) => tag.search(new RegExp(`${sTag}.`, 'i')) === 0));

console.log(result);
