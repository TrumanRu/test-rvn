import * as Handlebars from 'handlebars';

Handlebars.registerHelper('helperMissing', function (tag) {
  return `{{${tag.name}}}`;
});

const template = Handlebars.compile(
  "Name: {{name}}, missed: {{some}}",
  { noEscape: true },
);

const result = template(
  { name: "Nils" },
);
console.log(result);
// outputs:
// Name: Nils, missed: {{some}}
