const prefixRegexp = /^incident\.comments(?:[[.].+)?$/i

const tags = [
  'incident-comments',
  'incident.comment.',
  'incident.comments',
  'incident.comments.text',
  'incident.comments[0]',
  'incident.comments[0].text',
  'incident.comments.blablabla',
];

const result = tags.filter(tag => prefixRegexp.test(tag));

console.log(result);
